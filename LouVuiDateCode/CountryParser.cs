﻿using System;

namespace LouVuiDateCode
{
    public static class CountryParser
    {
        /// <summary>
        /// Gets a an array of <see cref="Country"/> enumeration values for a specified factory location code. One location code can belong to many countries.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <returns>An array of <see cref="Country"/> enumeration values.</returns>
        public static Country[] GetCountry(string factoryLocationCode)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            Country[] countries = new Country[1];
            switch (factoryLocationCode)
            {
                case "A0":
                case "A1":
                case "A2":
                case "AA":
                case "AAS":
                case "AH":
                case "AN":
                case "AR":
                case "AS":
                case "BA":
                case "BJ":
                case "BU":
                case "DR":
                case "DU":
                case "DT":
                case "CO":
                case "CT":
                case "CX":
                case "ET":
                case "MB":
                case "MI":
                case "NO":
                case "RA":
                case "RI":
                case "SF":
                case "SL":
                case "SN":
                case "SP":
                case "SR":
                case "TA":
                case "TJ":
                case "TH":
                case "TN":
                case "TR":
                case "TS":
                case "VI":
                case "VX":
                    {
                        countries[0] = Country.France;
                        break;
                    }

                case "SA":
                    {
                        Array.Resize(ref countries, 2);
                        countries[0] = Country.France;
                        countries[1] = Country.Italy;
                        break;
                    }

                case "LP":
                case "OL":
                    {
                        countries[0] = Country.Germany;
                        break;
                    }

                case "BO":
                case "CE":
                case "FO":
                case "MA":
                case "NZ":
                case "OB":
                case "PL":
                case "RC":
                case "RE":
                case "TD":
                    {
                        countries[0] = Country.Italy;
                        break;
                    }

                case "LW":
                    {
                        Array.Resize(ref countries, 2);
                        countries[0] = Country.France;
                        countries[1] = Country.Spain;
                        break;
                    }

                case "BC":
                    {
                        Array.Resize(ref countries, 2);
                        countries[0] = Country.Italy;
                        countries[1] = Country.Spain;
                        break;
                    }

                case "CA":
                case "LO":
                case "LB":
                case "LM":
                case "GI":
                case "UB":
                    {
                        countries[0] = Country.Spain;
                        break;
                    }

                case "DI":
                case "FA":
                    {
                        countries[0] = Country.Switzerland;
                        break;
                    }

                case "SD":
                case "FL":
                    {
                        Array.Resize(ref countries, 2);
                        countries[0] = Country.France;
                        countries[1] = Country.USA;
                        break;
                    }

                case "FC":
                case "FH":
                case "LA":
                case "OS":
                case "TX":
                    {
                        countries[0] = Country.USA;
                        break;
                    }

                default:
                    {
                        throw new ArgumentException("bad one");
                    }
            }

            return countries;
        }
    }
}
