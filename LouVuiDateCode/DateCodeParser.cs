﻿using System;
using System.Globalization;

namespace LouVuiDateCode
{
    public static class DateCodeParser
    {
        /// <summary>
        /// Parses a date code and returns a <see cref="manufacturingYear"/> and <see cref="manufacturingMonth"/>.
        /// </summary>
        /// <param name="dateCode">A three or four number date code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void ParseEarly1980Code(string dateCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length < 3 || dateCode.Length > 4)
            {
                throw new ArgumentException("DateCode is wrong.");
            }

            string year = "19" + Convert.ToString(dateCode[0], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[1], CultureInfo.CurrentCulture);
            string month;
            if (dateCode.Length == 4)
            {
                month = Convert.ToString(dateCode[2], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[3], CultureInfo.CurrentCulture);
            }
            else
            {
                month = Convert.ToString(dateCode[2], CultureInfo.CurrentCulture);
            }

            manufacturingYear = Convert.ToUInt32(year, CultureInfo.CurrentCulture);
            if (manufacturingYear < 1980 || manufacturingYear > 1986)
            {
                throw new ArgumentException("Year is wrong");
            }

            manufacturingMonth = Convert.ToUInt32(month, CultureInfo.CurrentCulture);
            if (manufacturingMonth < 1 || manufacturingMonth > 12)
            {
                throw new ArgumentException("Month is wrong");
            }
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingMonth"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A three or four number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void ParseLate1980Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length < 5 || dateCode.Length > 6)
            {
                throw new ArgumentException("DateCode is wrong");
            }

            string year = "19" + Convert.ToString(dateCode[0], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[1], CultureInfo.CurrentCulture);
            string month;
            if (dateCode.Length == 6)
            {
                month = Convert.ToString(dateCode[2], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[3], CultureInfo.CurrentCulture);
                factoryLocationCode = Convert.ToString(dateCode[4], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[5], CultureInfo.CurrentCulture);
            }
            else
            {
                month = Convert.ToString(dateCode[2], CultureInfo.CurrentCulture);
                factoryLocationCode = Convert.ToString(dateCode[3], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[4], CultureInfo.CurrentCulture);
            }

            factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);

            manufacturingYear = Convert.ToUInt32(year, CultureInfo.CurrentCulture);
            if (manufacturingYear < 1986 || manufacturingYear >= 1990)
            {
                throw new ArgumentException("Year is wrong");
            }

            manufacturingMonth = Convert.ToUInt32(month, CultureInfo.CurrentCulture);
            if (manufacturingMonth < 1 || manufacturingMonth > 12)
            {
                throw new ArgumentException("Month is wrong");
            }
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingMonth"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A six number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void Parse1990Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 6)
            {
                throw new ArgumentException("DateCode is wrong");
            }

            string year;

            if (dateCode[3] == '0')
            {
                year = "20" + Convert.ToString(dateCode[3], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[5], CultureInfo.CurrentCulture);
            }
            else
            {
                year = "19" + Convert.ToString(dateCode[3], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[5], CultureInfo.CurrentCulture);
            }

            string month;
            month = Convert.ToString(dateCode[2], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[4], CultureInfo.CurrentCulture);
            factoryLocationCode = Convert.ToString(dateCode[0], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[1], CultureInfo.CurrentCulture);

            factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);

            manufacturingYear = Convert.ToUInt32(year, CultureInfo.CurrentCulture);
            if (manufacturingYear < 1990 || manufacturingYear > 2006)
            {
                throw new ArgumentException("Year is wrong");
            }

            manufacturingMonth = Convert.ToUInt32(month, CultureInfo.CurrentCulture);
            if (manufacturingMonth < 1 || manufacturingMonth > 12)
            {
                throw new ArgumentException("Month is wrong");
            }
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingWeek"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A six number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingWeek">A manufacturing week to return.</param>
        public static void Parse2007Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingWeek)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 6)
            {
                throw new ArgumentException("DatCode is wrong");
            }

            string year = "20" + Convert.ToString(dateCode[3], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[5], CultureInfo.CurrentCulture);

            string week;
            week = Convert.ToString(dateCode[2], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[4], CultureInfo.CurrentCulture);
            factoryLocationCode = Convert.ToString(dateCode[0], CultureInfo.CurrentCulture) + Convert.ToString(dateCode[1], CultureInfo.CurrentCulture);

            factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);

            manufacturingYear = Convert.ToUInt32(year, CultureInfo.CurrentCulture);
            if (manufacturingYear < 2007)
            {
                throw new ArgumentException("Year is wrong");
            }

            manufacturingWeek = Convert.ToUInt32(week, CultureInfo.CurrentCulture);
            if (manufacturingWeek < 1 || manufacturingWeek > 53 || (manufacturingWeek > ISOWeek.GetWeeksInYear((int)manufacturingYear)))
            {
                throw new ArgumentException("Week is wrong");
            }
        }
    }
}
