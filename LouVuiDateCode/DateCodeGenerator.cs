﻿using System;
using System.Globalization;
using System.Text;

namespace LouVuiDateCode
{
    public static class DateCodeGenerator
    {
        /// <summary>
        /// Generates a date code using rules from early 1980s.
        /// </summary>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateEarly1980Code(uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingMonth > 12 || manufacturingYear > 1986 || manufacturingYear < 1980)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            StringBuilder res = new StringBuilder();

            res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[2]);
            res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[3]);
            res.Append(manufacturingMonth.ToString(CultureInfo.CurrentCulture));

            return res.ToString();
        }

        /// <summary>
        /// Generates a date code using rules from early 1980s.
        /// </summary>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateEarly1980Code(DateTime manufacturingDate)
        {
            if (manufacturingDate.Month > 12 || manufacturingDate.Year > 1986 || manufacturingDate.Year < 1980)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            StringBuilder res = new StringBuilder();

            res.Append(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[2]);
            res.Append(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[3]);
            res.Append(manufacturingDate.Month.ToString(CultureInfo.CurrentCulture));

            return res.ToString();
        }

        /// <summary>
        /// Generates a date code using rules from late 1980s.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateLate1980Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingMonth > 12 || manufacturingYear > 1989 || manufacturingYear < 1985)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2 || char.IsDigit(factoryLocationCode[0]) || char.IsDigit(factoryLocationCode[1]))
            {
                throw new ArgumentException("Factory location code is wrong");
            }

            StringBuilder res = new StringBuilder();

            res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[2]);
            res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[3]);
            res.Append(manufacturingMonth.ToString(CultureInfo.CurrentCulture));
            res.Append(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture));

            return res.ToString();
        }

        /// <summary>
        /// Generates a date code using rules from late 1980s.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateLate1980Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (manufacturingDate.Month > 12 || manufacturingDate.Year > 1989 || manufacturingDate.Year < 1985)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2 || char.IsDigit(factoryLocationCode[0]) || char.IsDigit(factoryLocationCode[1]))
            {
                throw new ArgumentException("Factory location code is wrong");
            }

            StringBuilder res = new StringBuilder();

            res.Append(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[2]);
            res.Append(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[3]);
            res.Append(manufacturingDate.Month.ToString(CultureInfo.CurrentCulture));
            res.Append(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture));

            return res.ToString();
        }

        /// <summary>
        /// Generates a date code using rules from 1990 to 2006 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate1990Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingMonth > 12 || manufacturingYear > 2006 || manufacturingYear < 1990)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2 || char.IsDigit(factoryLocationCode[0]) || char.IsDigit(factoryLocationCode[1]))
            {
                throw new ArgumentException("Factory location code is wrong");
            }

            StringBuilder res = new StringBuilder();

            res.Append(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture));
            if (manufacturingMonth.ToString(CultureInfo.CurrentCulture).Length == 1)
            {
                res.Append('0');
                res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[2]);
                res.Append(manufacturingMonth.ToString(CultureInfo.CurrentCulture)[0]);
                res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[3]);
            }
            else
            {
                res.Append(manufacturingMonth.ToString(CultureInfo.CurrentCulture)[0]);
                res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[2]);
                res.Append(manufacturingMonth.ToString(CultureInfo.CurrentCulture)[1]);
                res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[3]);
            }

            return res.ToString();
        }

        /// <summary>
        /// Generates a date code using rules from 1990 to 2006 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate1990Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (manufacturingDate.Month > 12 || manufacturingDate.Year > 2006 || manufacturingDate.Year < 1990)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2 || char.IsDigit(factoryLocationCode[0]) || char.IsDigit(factoryLocationCode[1]))
            {
                throw new ArgumentException("Factory location code is wrong");
            }

            StringBuilder res = new StringBuilder();

            res.Append(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture));
            if (manufacturingDate.Month.ToString(CultureInfo.CurrentCulture).Length == 1)
            {
                res.Append('0');
                res.Append(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[2]);
                res.Append(manufacturingDate.Month.ToString(CultureInfo.CurrentCulture)[0]);
                res.Append(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[3]);
            }
            else
            {
                res.Append(manufacturingDate.Month.ToString(CultureInfo.CurrentCulture)[0]);
                res.Append(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[2]);
                res.Append(manufacturingDate.Month.ToString(CultureInfo.CurrentCulture)[1]);
                res.Append(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[3]);
            }

            return res.ToString();
        }

        /// <summary>
        /// Generates a date code using rules from post 2007 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingWeek">A manufacturing week number.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate2007Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingWeek)
        {
            if (!DateTime.IsLeapYear((int)manufacturingYear) && manufacturingWeek >= 53 && manufacturingYear != 2015)
            {
                throw new ArgumentOutOfRangeException(nameof(factoryLocationCode));
            }

            if (manufacturingWeek > 53 || manufacturingYear < 2007 || manufacturingWeek < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(factoryLocationCode));
            }

            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2 || char.IsDigit(factoryLocationCode[0]) || char.IsDigit(factoryLocationCode[1]))
            {
                throw new ArgumentException("Factory location code is wrong");
            }

            StringBuilder res = new StringBuilder();

            res.Append(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture));
            if (manufacturingWeek.ToString(CultureInfo.CurrentCulture).Length == 1)
            {
                res.Append('0');
                res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[2]);
                res.Append(manufacturingWeek.ToString(CultureInfo.CurrentCulture)[0]);
                res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[3]);
            }
            else
            {
                res.Append(manufacturingWeek.ToString(CultureInfo.CurrentCulture)[0]);
                res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[2]);
                res.Append(manufacturingWeek.ToString(CultureInfo.CurrentCulture)[1]);
                res.Append(manufacturingYear.ToString(CultureInfo.CurrentCulture)[3]);
            }

            return res.ToString();
        }

        /// <summary>
        /// Generates a date code using rules from post 2007 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate2007Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (ISOWeek.GetWeekOfYear(manufacturingDate) > 53 || manufacturingDate.Year < 2007)
            {
                throw new ArgumentOutOfRangeException(nameof(factoryLocationCode));
            }

            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2 || char.IsDigit(factoryLocationCode[0]) || char.IsDigit(factoryLocationCode[1]))
            {
                throw new ArgumentException("Factory location code is wrong");
            }

            string month = Convert.ToString(manufacturingDate.Month, CultureInfo.CurrentCulture);
            string week = Convert.ToString(ISOWeek.GetWeekOfYear(manufacturingDate), CultureInfo.CurrentCulture);
            string year;
            if ((week == "52" || week == "53") && month == "1")
            {
                year = Convert.ToString(manufacturingDate.Year - 1, CultureInfo.CurrentCulture);
            }
            else
            {
                year = Convert.ToString(manufacturingDate.Year, CultureInfo.CurrentCulture);
            }

            StringBuilder res = new StringBuilder();

            if (week.Length == 1)
            {
                week = week.Insert(0, "0");
            }

            res.Append(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture));
            res.Append(week[0]);
            res.Append(year[2]);
            res.Append(week[1]);
            res.Append(year[3]);

            return res.ToString();
        }
    }
}
